#include <iostream>
#include "Node.h"
#include "CSLL.h"
using namespace std;

class CSLL;

int main()
{
    CSLL lista;
    lista.print();
    lista.append(59.9);
    lista.append(13.7);
    lista.append(10.0);
    lista.append(98.44);
    lista.append(16.7);
    lista.append(20.269);
    lista.append(1.5);
    lista.print();

    CSLL lista2 = lista;
    lista2.print();

    return 0;
}
