#ifndef NODE_H_INCLUDED
#define NODE_H_INCLUDED
#include <iostream>
using namespace std;

struct Node{
    double value;
    Node *prev;

    Node();
    Node(double value);
    Node(const Node& n);
    ~Node();
    void print() const;

};

#endif // NODE_H_INCLUDED
