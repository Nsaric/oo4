#include "Node.h"

Node::Node():prev(nullptr){}

Node::Node(double v):value(v) {}

Node::Node(Node const &c):value(value),prev(prev){}

Node::~Node(){
    cout << "Delete Node at: " << this << endl;
}

void Node::print() const {
    cout << "Node at: " << this << "; prev at: " << prev << "; value: " << value << endl;

}
