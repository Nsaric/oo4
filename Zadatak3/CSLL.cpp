#include "CSLL.h"

CSLL::CSLL(): tail(nullptr), head(nullptr){}

CSLL::CSLL(const CSLL &c):tail(new Node),head(new Node){
    Node *temp;
    head -> value = c.head -> value;
    tail -> value = c.tail -> value;
    //tail -> prev = tail;
    head -> prev = tail;
    temp = tail;
    for(Node *i = c.tail -> prev;i != c.head;i = i -> prev){
        Node *temp2 = new Node;
        temp2 -> value = i -> value;
        tail -> prev = temp2;
        tail = temp2;
        tail -> prev = head;
    }
    tail = temp;
}

CSLL::~CSLL(){
    for(Node *i = tail;i != head;){
        Node *temp = i -> prev;
        delete i;
        i = temp;
    }
    delete head;
}

void CSLL::prepend(double v){
    if(empty()){
        tail = new Node;
        head = tail;
        tail -> value = v;
        tail -> prev = tail;
    }
    else{
     Node *temp = head;
     head = new Node;
     head -> value = v;
     head -> prev = tail;
     temp -> prev = head;
    }
}

void CSLL::append(double v){
    if(empty()){
        tail = new Node;
        head = tail;
        tail -> value = v;
        tail -> prev = tail;
    }
    else{
        Node *temp = tail;
        tail = new Node;
        tail -> value = v;
        tail -> prev = temp;
        head -> prev = tail;
    }
}

double CSLL::removeFromTail(){
    double v = tail -> value;
    Node *temp = tail -> prev;
    delete tail;
    tail = temp;
    head -> prev = tail;
    return v;
}

void CSLL::print() const{
    cout << "Head at: " << head << "; tail at: " << tail << endl;
    if(!empty()){
        Node *i = tail;
        i -> print();
        for(i = tail -> prev;i != tail;i = i -> prev){
            i -> print();
        }
        cout << endl;
    }
}

bool CSLL::empty() const { return tail == nullptr; }
