#ifndef CSLL_H_INCLUDED
#define CSLL_H_INCLUDED
#include "Node.h"

class CSLL{
protected:
    Node *tail;
    Node *head;
public:
    CSLL();
    CSLL(const CSLL&);
    ~CSLL();

    bool empty() const;

    void prepend(double);
    void append(double);

    double removeFromTail();
    void print() const;

};

#endif // CSLL_H_INCLUDED
