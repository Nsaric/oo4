#include <iostream>
#include <list>
using namespace std;

double suma(list<double> &L)
{
    double sum = 0.0;
    for(auto &elem : L)
        sum = sum + elem;
    
    return sum;
}

double produkt(list<double> &L)
{
    double p = 1.0;
    for(auto &elem : L) p = p * elem;
    return p;

}

double maksimalni(list<double> &L)
{
    double max = L.front();
    for(auto &elem : L)
    {
        if(elem>max) max = elem;
    }
    return max;
}

double minimalni(list<double> &L)
{
    double min = L.front();
    for(auto &elem : L)
    {
        if(elem<min) min = elem;
    }
    return min;
}


int main()
{
    list<double> L = {59.9, 13.7, 10.0, 98.44, 16.7, 20.269, 1.5};
    cout << "Suma: " << suma(L) << endl;
    cout << "Produkt: " << produkt(L) << endl;
    cout << "Minimalni: " << minimalni(L) << endl;
    cout << "Maximalni: " << maksimalni(L) << endl;
    return 0;
}