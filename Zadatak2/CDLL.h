#ifndef CDLL_H
#define CDLL_H

#include "node.h"

class CDLL
{
protected:
    Node* head;
    Node* tail;
public:
    CDLL();
    CDLL(const CDLL& c);
    ~CDLL();
    bool empty() const; 
    void prepend(double value); //postavi vrijednost na početak
    void append(double value); //postavi vrijednsot na kraj
    double removeFromHead(); //ukloni čvor s početka
    double removeFromTail(); //ukloni čvor s kraja
    void print() const;
    void sort();
};

#endif
    