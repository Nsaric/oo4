#ifndef NODE_H
#define NODE_H

struct Node
{
    double value;
    Node* next;
    Node* prev;
    Node();
    Node(double value);
    Node(const Node& n);
    ~Node();
    void swap(Node& n);
    void print() const;
};

#endif