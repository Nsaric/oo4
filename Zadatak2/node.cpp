#include "Node.h"
#include <iostream>
using namespace std;

Node::Node()
{
    prev = nullptr;
    next = nullptr;
    value = 0.0;
}

Node::Node(double value)
{
    next = nullptr;
    prev = nullptr;
    value = value;
}

Node::Node(const Node& n)
{
    next = n.next;
    prev = n.prev;
    value = n.value;
}

Node::~Node() = default;

void Node::swap(Node &n)
{
    
}

void Node::print() const
{
    cout << "Node at" << this << ";value = " << value << "next at" << next << "prev at" << prev << endl;

}