#include "CDLL.h"

CDLL::CDLL()
{
    head = nullptr;
    tail = nullptr;
}
CDLL::CDLL(const CDLL& c)
{
    Node* it{c.head};
    do{
        append(it->value);
        it = it->next;
    }while (it != c.head);
}

CDLL::~CDLL()
{
    while(!empty())
        removeFromHead();
}

bool CSLL::empty() const {return head==nullptr}
